﻿component {
	
	this.name='mxunit-testbuilder_' & hash(getCurrentTemplatePath());
	this.applicationtimeout = createTimeSpan(0,0,10,0);
	this.sessionmanagement = false;
	
	public void function onError(
		struct exception
		,string eventname
	) {
		writeDump(arguments);
		abort;
	}
}