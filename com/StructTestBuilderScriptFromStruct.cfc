
component {

/*
20201121    Adapt for use with MXUnit Test Builder?
    The -MakeTestsForStruct- functionality would benefit from a reversal engine
        In other words, pass in a struct and receive a format that one could 
        plop into -MakeTestsForStruct- for test generation.
    http://rapidfire/mxunit-testbuilder/MakeTestsForStruct.cfm

                                            based on isValid(mType)
    [
          {mName : "myArrayWithLength"      , mType : "array"               , mHasLengthOrVal : true    }
        , {mName : "myStringWithLength"     , mType : "string"              , mHasLengthOrVal : true    }
        , {mName : "myObj"                  , mType : "my.path.to.myObj"    , mHasLengthOrVal : true    }
        , {mName : "myNumericWithLength"    , mType : "numeric"             , mHasLengthOrVal : true    }
        , {mName : "myUUID"                 , mType : "uuid"                , mHasLengthOrVal : true    }
        , {mName : "myGuid"                 , mType : "guid"                , mHasLengthOrVal : true    }
        , {mName : "myXml"                  , mType : "xml"                 , mHasLengthOrVal : true    }
        , {mName : "myBoolean"              , mType : "boolean"             , mHasLengthOrVal : true    }
        , {mName : "myDate"                 , mType : "date"                , mHasLengthOrVal : true    }
        , {mName : "myQuery"                , mType : "query"               , mHasLengthOrVal : true    }
        , {mName : "myStruct"               , mType : "struct"              , mHasLengthOrVal : true    }
        , {mName : "mybinary"               , mType : "binary"              , mHasLengthOrVal : true    }
        , {mName : "myArraySansLength"      , mType : "array"                                           }
        , {mName : "myStringSansLength"     , mType : "string"                                          }
        , {mName : "myObjSansLength"        , mType : "my.path.to.myObj"                                }
        , {mName : "myNumericSansLength"    , mType : "numeric"                                         }
        , {mName : "myUUIDSansLength"       , mType : "uuid"                                            }
        , {mName : "myGuidSansLength"       , mType : "guid"                                            }
        , {mName : "myXmlSansLength"        , mType : "xml"                                             }
        , {mName : "myBooleanSansLength"    , mType : "boolean"                                         }
        , {mName : "myDateSansLength"       , mType : "date"                                            }
        , {mName : "myQuerySansLength"      , mType : "query"                                           }
        , {mName : "myStructSansLength"     , mType : "struct"                                          }
        , {mName : "mybinarySansLength"     , mType : "binary"                                          }
    ]
*/




/*
I am used to create a string to use for documenting a struct.

    debug(new rcv.utils.StringDescForStruct().introspectStruct(server));
    OUTPUTS the string: 
{
    'coldfusion': lucee.runtime.type.ReadOnlyStruct,
    'os': lucee.runtime.type.ReadOnlyStruct,
    'lucee': lucee.runtime.type.ReadOnlyStruct,
    'separator': lucee.runtime.type.ReadOnlyStruct,
    'java': lucee.runtime.type.ReadOnlyStruct,
    'servlet': lucee.runtime.type.ReadOnlyStruct,
    'system': lucee.runtime.type.ReadOnlyStruct,
    'A0608BEC-0AEB-B46A-0E1E1EC5F3CE7C9C.1.0': java.net.URLClassLoader,
    '_regExPattern_F8301AA518EF6F12F087CA5E8F63574B': java.lang.Class,
    '_regExPattern_AA49163D1E472E60B5CC5283B763D75B': java.lang.Class,
}

*/

    public any function init() {
        return this;
    }


    private string function transpose(
        required string mType
    ) {
        var ret = '';
        switch(arguments.mType) {
            case 'lucee.runtime.type.StructImpl': ret = 'struct'; break;
            case 'lucee.runtime.type.ArrayImpl': ret = 'array'; break;
            case 'lucee.runtime.type.dt.DateTimeImpl': ret = 'date'; break;
            case 'java.lang.Boolean': 			ret = 'boolean'; break;
            case 'java.util.Date' :				ret = 'date'; break;
            case 'java.lang.String' :			ret = 'string'; break;
            case 'java.lang.Integer' :			ret = 'numeric'; break;
            case 'java.lang.Long' :				ret = 'numeric'; break;
            case 'java.lang.Double' :			ret = 'numeric'; break;
            case 'com.mongodb.BasicDBList': 	ret = 'array'; break;
            case 'com.mongodb.BasicDBObject': 	ret = 'struct'; break;
            case 'org.bson.types.ObjectId': 	ret = 'any'; break;
            default: 							ret=arguments.mType;
        }
        return ret;
    } 
    
        
    public string function introspectStruct(
         required struct s
    ) {
        var outArray=[];
        
        for(var key in arguments.s) {
            var item = {};
            try {                                // FIXME: getMetaData().getName() - different results across ACF & Lucee https://www.trycf.com/scratch-pad
                item.type = this.transpose( getMetaData(s[key]).getName() );
            }
            catch(any e) {
                item.type = this.transpose( getMetaData(s[key]) );
            }
            item.name = key;


            // its a struct
            if(item.type == 'struct') {
                item.value = this.introspectStruct(arguments.s[key]);
            }

            // its an array
            else if(item.type == 'array') {
                item.value = [];
                // loop over the items in the array
                for(var k in arguments.s[key]) {
                    
                                        // FIXME: getMetaData().getName() - different results across ACF & Lucee https://www.trycf.com/scratch-pad
                    if( this.transpose( getMetaData(k).getName() ) == 'struct') {
                        arrayAppend(item.value, this.introspectStruct(k));
                    }
                    else {
                        //arrayAppend(item.value, k);
                    }
                }
            }

            // just add it!
            //else {
                //item.value = arguments.s[key];
                //item.type = this.transpose( item.type );
            //}

            arrayAppend(outArray, convertForOutput(item));
        }
        return convertForOutput(outArray);
    }


public string function convertForOutput(
    required any complexVal             // could be struct or array
) {
    var out='';
    var myVal = duplicate(arguments.complexVal);

/* 
FIXME: difference between struct & component/CFC/Object
       STRUCT:  (isValid('struct', server) == true && isValid('component', server) == false)
       OBJECT:  (isValid('struct', server) == true && isValid('component', server) == true)
*/

    if(isStruct(myVal)) {
        //var out='{ ';

                    // TODO: myVal.name is for an object after getMetaData(object)
                                // otherwise, those keys likely dont exist
                                
        out &= " '#myVal.name#': #myVal.type#";
        //writeDump(var={'name':arguments.complexVal.name, 'type':arguments.complexVal.type}, output="console");
         /* for(var key in arguments.complexVal) {
            try {
                out &= " '#key.name#': #key.type#, ";
            }
            catch(any e) {
                

            }

        }  */
        //out &= '}';
    }
    else if(isArray(myVal)) {
        // should be an array of structs
        out &= '{ ';
        for(var itm in myVal) {
            out &= itm & ', ';
        }
        out &= ' }';
    }
    return out;
}


	/*
	 * Sample struct to be passed in:
	 * Struct
	    access string public
	    closure boolean false
	    description string
	    name string getContent
	    output boolean true
	    owner string C:\apache-tomcat-7.0.32\webapps\ROOT\mxunit-testbuilder\com\DataService.cfc
	    parameters Array
	        1 Struct
	            name string catid
	            required boolean true
	            type string any
	    returnFormat string wddx
	    returntype string query

	 From: http://help.adobe.com/en_US/ColdFusion/9.0/Developing/WSc3ff6d0ea77859461172e0811cbec22c24-7e10.html#WSc3ff6d0ea77859461172e0811cbec0a66e-7ff6
	 The valid standard returnType names are:

	 FUNCTION ret types	ISVALID argument types
	 any,				isValid(any) any simple value. Returns false for complex values, such as query objects;; equivalent to the IsSimpleValue function.
	 array, 			isValid(array)
	 binary, 			isValid(binary)
	 Boolean, 			isValid(boolean)
	 date, 				isValid(date)
	 guid, 				isValid(guid)
	 numeric, 			isValid(INTEGER)
	 query, 			isValid(query)
	 string, 			isValid(string)
	 struct, 			isValid(struct)
	 uuid, 				isValid(uuid)
	 variableName, 		isValid(variableName) a string formatted according to ColdFusion variable naming conventions.
	 xml,				isXML()
	 void 				isNull()

	 If you specify any other name, ColdFusion requires the argument to be a ColdFusion component with that name.
	 SOMECOMPNAME		isInstanceOf(obj, 'path.to.component')



     public string function getIsValidLineForReturnType(
        required string variableName hint="name of the variable to be tested"
      , required string functionReturnType hint="array,binary,boolean,date,guid,query,string,struct,uuid,variableName,any,numeric,xml,void,component"
      ) {
      if(!len(arguments.variableName)) {
          throw(message='Argument variableName to getIsValidLineForReturnType() should not be an empty string.', type='custom');
      }
      else if(!len(arguments.functionReturnType)) {
          throw(message='Argument functionReturnType to getIsValidLineForReturnType() should not be an empty string.', type='custom');
      }

      var t=arguments.functionReturnType;
      var ret='';
      var useReturnTypeAnyStatement = false;
      var useIsXml = false;
      var useIsNull = false;
      var useIsComponent = false;

      if (!arrayFindNoCase(['array','binary','boolean','date','guid','query','string','struct','uuid','variableName'],t)) {
          switch(t) {
              case 'any': useReturnTypeAnyStatement=true; break;
              case 'numeric':
                  t = 'INTEGER';
                  break;
              case 'datetime': t='date';break;
              case 'xml': useIsXml = true; break;
              case 'void': useIsNull = true; break;
              default: useIsComponent = true; break;
          }
      }

      // if there are no special conditions
      if (
             !useReturnTypeAnyStatement
          && !useIsXml
          && !useIsNull
          && !useIsComponent
          ) {

          ret='assertTrue(isValid("'
              & t
              & '",'
              & arguments.variableName
              & '), "Invalid datatype. Expected '
              & t
              & '");'
              ;
      }
      else if(useReturnTypeAnyStatement) {
          ret='// return type for the function was ANY';
      }
      else if(useIsXml) {
          ret='assertTrue(isXML(#arguments.variableName#),"failed isXML() validation.");';
      }
      else if(useIsNull) {
          ret='assertTrue(isNull(#arguments.variableName#),"Value returned was not VOID");';
      }
      else if(useIsComponent) {
          ret = 'assertTrue(isObject(#arguments.variableName#),"#arguments.variableName# is not an object as expected");'; //isObject
          ret &= newLine & newTab;
          ret &= 'assertTrue(isInstanceOf(#arguments.variableName#,"#t#"),"#arguments.variableName# !isInstanceOf(#t#)");'; //isInstanceOf
      }
      return ret;
  }
	 **/




}