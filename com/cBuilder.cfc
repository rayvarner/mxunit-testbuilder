component {

	variables.newLine = 			chr(10);// & chr(13);
	variables.newTab = 				chr(9);
	variables.newAst = 				chr(42);
	variables.newSemiColon = 		chr(59);
	variables.newDoubleQuote = 		chr(34);
	variables.newLT  =				chr(60);
	variables.newGT = 				chr(62);

	//variables.newForwSlash = 		chr(47);
	//variables.compPageencoding = 	"Cp1252";

	variables.failAllTests = 				true; 							// if true, ALL generated tests will start with fail('not yet implemented')
	variables.compExtends = 				"mxunit.framework.TestCase";	//"test.wc.base";  // Ancestor of the test cfc generated.
	variables.useFactoryToCreateObject = 	false;							// default is to use "new" to create an obj. This true uses "factory.get('nameOfComponentInFactory')"
	variables.customTestURL = 				'';								// see THIS.getTestURL() for info - ex: some.path.to.component, getTestURL() returns "/test/some/path/to/"
	variables.compPath = 					'mxunit-testbuilder.com.ReturnTypesTester';		// the dotpath to the component to be introspected.


	public any function init() {
		return this;
	}





	/**
	* @hint = I return a struct containing the metadata of a component. NOTE: a component with no content will still have a struct with keys returned
	*/
	public struct function getTheMetaData(
		required string compPath	hint = "dotpath to the component"
	) {
		if(len(arguments.compPath)) {
			try {
				return getComponentMetaData(arguments.compPath);
			}
			catch(any e) {
				throw( message=e.message, type='custom');
			}
		} else {
			throw( message='component path cannot be an empty string', type='custom');
		}
	}





	/**
	* @hint = I write the filesystem IF the file does not exist. ForceOverwrite argument avail.
	* 			I return an empty string if successful
	*/
	public string function writeTheFile(
		required string filePathAndName
		, required string data
		, boolean forceOverwrite = false
		, string charset
	) {
		var ret = '';
		if(fileExists(arguments.filePathAndName) && arguments.forceOverwrite == false) {
			ret = 'File exists. Use forceOverwrite argument if you want to write it anyway.';
		}
		else {
			try {
				fileWrite(arguments.filePathAndName, arguments.data);
			}
			catch(any e) {
				ret &= e.message & ' ' & e.detail;
			}
		}
		return ret;
	}




	public struct function doWriteToFileSystem(
		required string fileContent					hint = "file content to be written"
		, string compPath = this.getCompPath() 		hint = "dot path to the component"
		, string customTestURL = ''					hint = "ability to override the default value from getTestURL()"
		, boolean forceOverwrite = false
	) {
		if(!len(arguments.fileContent)) {
			throw('File content not provided.','custom');
		}
		var ret = {
			  'file_write_result': ''
			, 'canonical_path_and_file': ''
			, 'http_url': ''
		};
		if(len(arguments.customTestURL)) {
			this.setCustomTestURL(arguments.customTestURL);
		}
		var myUrl = this.getTestURL(arguments.compPath); //  /test/mxunit-testbuilder/com/testReturnTypesTester?method=runtestremote&output=html
		var myPath = listFirst(myUrl,'?'); // /test/mxunit-testbuilder/com/testReturnTypesTester
		var myFileName = listLast(myPath, '/') & '.cfc'; // testReturnTypesTester.cfc

// TODO: test with Linux
		// server.os.name=='Linux'		server.separator.file=='/'
		// server.os.name=='Windows 10'	server.separator.file=='\'

		// windows path
		myPath = replace(myPath,'/','\','all');

		ret.canonical_path_and_file = expandPath(  myPath );

		ret.file_write_result = this.writeTheFile(ret.canonical_path_and_file, arguments.fileContent, arguments.forceOverwrite);
// TODO: Verify there's no issue with an additional "s" with 
   // http://s127.0.0.1:63557/mxunit-testbuilder/unittest/com/testTestReturnTypes.cfc?method=runtestremote&output=html
/*
Lucee 5.x appears to use 'on' and 'off' for cgi.https.
Lucee4.5 & ACF2018 uses empty string for cgi.https being off.
*/
		ret.http_url = "http://#(cgi.SERVER_PORT=='443'?'s':'')##cgi.sERVER_NAME#";
		if(cgi.server_port != 80) {
			ret.http_url &= ':#cgi.server_port#';
		}
		ret.http_url &= myUrl;
		return ret;
	}




	/**
	* @hint = process() is the primary function for generating the test cases.
	*/
	public struct function process(
		boolean skipInit = true 				hint = "if the function is named INIT, we do not want a test case for it"
		, string compPath = this.getCompPath()
		, string customTestURL = ''				hint = "ability to override the default value from getTestURL()"
		, string compExtends 					hint = "default is mxunit.framework.TestCase"
		, boolean writeToFileSystem = false
		, boolean forceOverwrite = false
		, boolean useFactoryToCreateObject
	) {
		if(structKeyExists(arguments, 'useFactoryToCreateObject')) {
			this.setUseFactoryToCreateObject(arguments.useFactoryToCreateObject);
		}
		if(structKeyExists(arguments, 'compExtends') && len(trim(arguments.compExtends))) {
			this.setCompExtends(arguments.compExtends);
		}
		if(len(arguments.customTestURL)) {
			this.setCustomTestURL(arguments.customTestURL);
		}

		var fileContent='';
		var comp=getTheMetaData(arguments.compPath);
		var i=0;

		//build the prolog
		fileContent &= getProlog(arguments.compPath);
		// build the setup
		fileContent &= getSetup(arguments.compPath);
		// build the teardown
		fileContent &= getTeardown();

		if (
			structKeyExists(comp,'FUNCTIONS')
			&& isArray(comp['FUNCTIONS'])
			&& arrayLen(comp['FUNCTIONS'])
			) {
			var testcases='';

			for(i=1;i <= arrayLen(comp['FUNCTIONS']); i++) {
				var f=comp['FUNCTIONS'][i];
				// if the function is named INIT, we do not want a test case for it
				if(arguments.skipInit && f['NAME'] == 'init') {
					continue;
				}
				testcases &= this.createFunctionFromStruct(f, arguments.compPath);
			}
			fileContent &= testcases;
		}

		// build the epilog
		fileContent &= getEpilog();

		var ret = {
			'fileContent': fileContent
			, 'WriteFile': {}
		};

		if(arguments.writeToFileSystem) {
			ret['WriteFile'] = this.doWriteToFileSystem(
				  fileContent = fileContent
				, compPath = arguments.compPath
				, forceOverwrite = arguments.forceOverwrite
			);
		}
		return ret;
	}









	/*
	If getCustomTestURL() is an empty string, a value will be generated.
		The default is a subfolder named "test" followed by the folder path of the component.
		ex: some.path.to.component, getTestURL() returns "/test/some/path/to/"
TODO: validate for a legal compPath. No bad chars, etc.
	 */
	public string function getTestURL(
		required string compPath	hint = "dotpath to the component. ex: some.path.to.component"
	) {
		
		// name of the component with 'test' prefixed. ex: testComponent
		var mComponent = 'test' & capFirstLetter(this.extractObjName(arguments.compPath));
		
		// use the default method of building up the url. ex: '/test/some/path/to/'
		if(!len(this.getCustomTestURL())) {
			var mUrl = '/test/';
			var pathArray = listToArray(arguments.compPath,'.');
			if(arrayLen(pathArray)>1) {
				// we do not want the last item
				for(var i=1;i<arrayLen(pathArray);i++) {
					mUrl &= pathArray[i] & '/';
				}
			}
		}
		// else - this.getCustomTestURL() returns something like: '/some/UNITTESTING/path/to/component/' or whatever was assigned.
		else {
			var mUrl = this.getCustomTestURL();
		}
		
		// concat the url & component
		return mUrl & mComponent & '.cfc?method=runtestremote&output=html';
	}









	public string function getProlog(
		required string compPath	hint = "dotpath to the component"
	) {
		var ret = '/' & newAst & newAst & newLine
			& newAst & ' ' & getTestURL(arguments.compPath) & newLine
			;

		if(getUseFactoryToCreateObject()) {
			ret &= newAst & newLine & newAst & ' variables.factory is created in the ' & getCompExtends() & ' component THIS extends.' & newLine
				& newAst & ' Its necessary for using the mxunit plugin for eclipse' & newLine
				;
		}
		ret &= newAst & '/' & newLine;
		ret	&= 'component extends='
			& newDoubleQuote & getCompExtends() & newDoubleQuote
			& ' {' & newLine
			& newTab & '// These tests were generated by script! PLEASE VERIFY prior to executing!'
			& newLine & newLine;
		return ret;
	}










	public string function getEpilog() {
		return  newLine & newLine & newLine &'}';
	}











/**
I am not in use but kept in case the tag version is required
public string function getProlog_withTags() {
	var ret='';
	ret &= '#newLT#cfcomponent extends=' & newDoubleQuote & getCompExtends() & newDoubleQuote & newGT
				& newLine & '#newLt#!--- These tests were generated by script! PLEASE VERIFY prior to executing! ---#newGT#' & newLine & newTab & newTab
				& newLine & newTab & '#newLT#cfscript#newGT#' & newLine & newTab & newTab;
	ret &= newLine & newLine;
	return ret;
}

*/






/**
I am not in use but kept in case the tag version is required
public string function getEpilog_withTags() {
	return  newLine & newLine & '#newTab##newLT#/cfscript#newGT##newLine##newLT#/cfcomponent#newGT#';
}
*/








/*
I return the setup() function for the generated text
*/
	public string function getSetup(
		required string compPath	hint = "dotpath to the component"
	) {
		var ret='';
		ret &= 'public void function setup() {';
		ret &= newLine;
		ret &= newTab;
		ret &= "super.setup()#newSemiColon#";
		ret &= newLine;
		ret &= newTab;

		var objName = extractObjName(arguments.compPath);

		//create the component under test
		ret &= (!getUseFactoryToCreateObject()) ? '// ' : '';
		ret &= "#objName# = factory.get('";

		var tmpPath = listLast(arguments.compPath,'.');
		// if the path contains dashes, the component must be enclosed in quotes
		if (!find('-',objName)) {
			ret &= objName;
		}
		else {
			ret &= '"' & objName & '"';
		}
		ret &= "');";
		ret &= newLine;

		//create the component under test
		ret &= (getUseFactoryToCreateObject()) ? '#newTab#// ' : newTab;
		ret &= "#objName# = new ";

		// if the path contains dashes, the component must be enclosed in quotes
		if (!find('-',arguments.compPath)) {
			ret &= arguments.compPath;
		}
		else {
			ret &= '"' & arguments.compPath & '"';
		}
		ret &= "(); // TODO: add arguments to #objName# as necessary";
		ret &= newLine;
		ret &= '}' & newLine & newLine;
		return ret;
	}











	public string function getTeardown() {
		var ret = 'public void function teardown() {}' & newLine;
		ret &= '#newLine#// =============================================================================';
		ret &= '#newLine#// == Test Cases ===============================================================';
		ret &= '#newLine#// =============================================================================';
		ret &= newLine;
		return ret;
	}









	/*
	 * Sample struct to be passed in:
	 * Struct
	    access string public
	    closure boolean false
	    description string
	    name string getContent
	    output boolean true
	    owner string C:\apache-tomcat-7.0.32\webapps\ROOT\mxunit-testbuilder\com\DataService.cfc
	    parameters Array
	        1 Struct
	            name string catid
	            required boolean true
	            type string any
	    returnFormat string wddx
	    returntype string query

	 From: http://help.adobe.com/en_US/ColdFusion/9.0/Developing/WSc3ff6d0ea77859461172e0811cbec22c24-7e10.html#WSc3ff6d0ea77859461172e0811cbec0a66e-7ff6
	 The valid standard returnType names are:

	 FUNCTION ret types	ISVALID argument types
	 any,				isValid(any) any simple value. Returns false for complex values, such as query objects;; equivalent to the IsSimpleValue function.
	 array, 			isValid(array)
	 binary, 			isValid(binary)
	 Boolean, 			isValid(boolean)
	 date, 				isValid(date)
	 guid, 				isValid(guid)
	 numeric, 			isValid(INTEGER)
	 query, 			isValid(query)
	 string, 			isValid(string)
	 struct, 			isValid(struct)
	 uuid, 				isValid(uuid)
	 variableName, 		isValid(variableName) a string formatted according to ColdFusion variable naming conventions.
	 xml,				isXML()
	 void 				isNull()

	 If you specify any other name, ColdFusion requires the argument to be a ColdFusion component with that name.
	 SOMECOMPNAME		isInstanceOf(obj, 'path.to.component')


	 **/

	 // TODO: add 'closure' as a type
	public string function getIsValidLineForReturnType(
		  required string variableName 				hint="name of the variable to be tested"
		, required string functionReturnType 		hint="array,binary,boolean,date,guid,query,string,struct,uuid,variableName,any,numeric,xml,void,component"
		) {
		if(!len(arguments.variableName)) {
			throw(message='Argument variableName to getIsValidLineForReturnType() should not be an empty string.', type='custom');
		}
		else if(!len(arguments.functionReturnType)) {
			throw(message='Argument functionReturnType to getIsValidLineForReturnType() should not be an empty string.', type='custom');
		}

		var t=arguments.functionReturnType;
		var ret='';
		var useReturnTypeAnyStatement = false;
		var useIsXml = false;
		var useIsNull = false;
		var useIsComponent = false;

		if (!arrayFindNoCase(['array','binary','boolean','date','guid','query','string','struct','uuid','variableName'],t)) {
			switch(t) {
				case 'any': useReturnTypeAnyStatement=true; break;
				case 'numeric':
					t = 'INTEGER';
					break;
				case 'datetime': t='date';break;
				case 'xml': useIsXml = true; break;
				case 'void': useIsNull = true; break;
				default: useIsComponent = true; break;
			}
		}

		// if there are no special conditions
		if (
			   !useReturnTypeAnyStatement
			&& !useIsXml
			&& !useIsNull
			&& !useIsComponent
			) {

			ret='assertTrue(isValid("'
				& t
				& '",'
				& arguments.variableName
				& '), "Invalid datatype. Expected '
				& t
				& '");'
				;
		}
		else if(useReturnTypeAnyStatement) {
			ret='// return type for the function was ANY';
		}
		else if(useIsXml) {
			ret='assertTrue(isXML(#arguments.variableName#),"failed isXML() validation.");';
		}
		else if(useIsNull) {
			ret='assertTrue(isNull(#arguments.variableName#),"Value returned was not VOID");';
		}
		else if(useIsComponent) {
			ret = 'assertTrue(isObject(#arguments.variableName#),"#arguments.variableName# is not an object as expected");'; //isObject
			ret &= newLine & newTab;
			ret &= 'assertTrue(isInstanceOf(#arguments.variableName#,"#t#"),"#arguments.variableName# !isInstanceOf(#t#)");'; //isInstanceOf
		}
		return ret;
	}












	public string function createFunctionFromStruct(
		required struct fnMetadata
		, required string compPath	hint = "dotpath to the component"
		) {
		// begin refining the struct that will contain the text to be written
		var s={};
		var f=duplicate(arguments.fnMetadata);
		var ret='';
		
		// ACF does not include returnType by default
		if(!structKeyExists(f, 'RETURNTYPE')) {
			f['RETURNTYPE'] = 'any';
		}

		if(!find('.',f['RETURNTYPE'])) {
			var s.returnvar = 'shouldBe'
				& capFirstLetter(f['RETURNTYPE'])
				;
		}
		else {
			var s.returnvar = listLast(f['RETURNTYPE'],'.');
		}


		// Create the comment
		ret &= newline;
		ret &= "#newLine#// ==============================================";
		ret &= "#newLine#// == #f.name# ";
		ret &= (structKeyExists(f, 'access') && isSimpleValue(f.access) && len(f.access)) ? "(#f.access#)" : "";

/*
Omitting HINT & DESCRIPTION at this time. Multi-line text can be problematic.
*
		if(structKeyExists(f,'HINT') && len(f['HINT'])) {
			ret &= "#newLine#// ==== hint: #f.hint# ";
		}
		if(structKeyExists(f,'DESCRIPTION') && len(f['DESCRIPTION'])) {
			ret &= "#newLine#// ==== description: #f.description# ";
		}
*/
		ret &= "#newLine#// ===============================================";
		ret &= newline;

		// function declaration
		ret &= 'public void function test'
				& capFirstLetter(f['NAME'])
				& '() {'
				;

		if(variables.failAllTests) {
			ret &= newLine & newTab & "fail('not yet implemented')" & newSemiColon & newLine;
		}

		// Create arguments to the function under test if there are any
		if(
			structKeyExists(f,'PARAMETERS')
			&& arrayLen(f['PARAMETERS'])
			) {
				ret &= newLine & newTab &  createFunctionArgs(f['PARAMETERS']);
		}
		ret &= newline;

		// If a function is private, use makePublic() within mxunit
		if(structKeyExists(f, 'access') &&  f.access == 'private') {
			ret &= newTab;
			ret &= 'super.makePublic(#extractObjName(arguments.compPath)#, "#f.name#", "_#f.name#");';
			f.name = '_' & f.name;
			ret &= newline;
		}

		// Create the line to call the function under test
		ret &= newTab;
		ret &= 'var '
				& s.returnvar
				& ' = #extractObjName(arguments.compPath)#.'
				& f.name
				& '('
				;

		// if the function takes params, add
		if(
			structKeyExists(f,'PARAMETERS')
			&& arrayLen(f['PARAMETERS'])
			) {
				ret &= 'argumentcollection=args';
		}
		ret &=');';
		ret &= newline;

		// Add a commented debug;
		ret &= newTab;
		ret &= "// debug(" & s.returnvar & ");";
		ret &= newline;

		// Add the default assertion
		ret &= newTab;
		ret &= this.getIsValidLineForReturnType( variableName=s['RETURNVAR'], functionReturnType=f['RETURNTYPE'] );
		ret &= newline;

		// end the function
		ret &= '}';
		ret &= newline & newLine;
		return ret;
	}










	/*
	 *
	* @hint = I take a ComponentMetaDatas function paramaeters and assemble a string representation.
		argArray: an array of structs for ComponentMetaData.functions[x].parameters

		the struct (of a function argument) should have keys of
		NAME, REQUIRED ... but may have TYPE, HINT, DEFAULT
			from http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-7e47.html
	 */
	public string function createFunctionArgs(
		required array argArray 		hint="[{name:string, required:bool, type:string}, ...]"
		) {
		 var ret = '';
		 ret &= newline & newline & newTab;

		 ret='var args = {';
		 ret &= newline;

		 var counter=1;
		 var s={};

		 for(var i=1; i <= arrayLen(arguments.argArray); i++) {
 		 	s = {};
			s = arguments.argArray[i];
 		 	ret &= newTab
 		 		& newTab
 		 		;
 		 	ret &= getArgumentLine(s,(counter != 1));
 		 	if (counter < arrayLen(arguments.argArray)) {
 		 		ret &= newline;
 		 	}
	 		counter++;
	 	}
		 ret &= newline & newTab;
		 ret &= '};';
		 return ret;
	}










	/*
	the struct (of a function argument) should have keys of
	NAME, REQUIRED ... but may have TYPE, HINT, DEFAULT
		from http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-7e47.html
	*/
	public string function getArgumentLine(
		required struct mStruct
		, boolean includeComma = true
		) {
		var ret = '';
		var s = arguments.mStruct;

		// REQUIRED t/f
		// if not required, use commented?
		param name="s.required" default="false";
		param name="s.name" default="";

		if (!s.required) {
			ret &= '// ';
		}
		if (arguments.includeComma) {
			ret &= ', ';
		}

		// default the value of the argument to an empty string
	 	ret &= s['NAME'] & ' : ';

		if (structKeyExists(s, 'TYPE')) {
			// strings
			if(listContains( 'string,uuid,guid,xml', s['TYPE'])) {
				ret &= '#newDoubleQuote##newDoubleQuote#  // ';
			}
			// query
			else if(s['TYPE']=='query') {
				ret &= "queryNew('') // ";
			}
			// array
			else if(s['TYPE']=='array') {
				ret &= "[] // ";
			}
			// struct
			else if(s['TYPE']=='struct') {
				ret &= "{} // ";
			}
			// numeric
			else if(s['TYPE']=='numeric') {
				ret &= "0 // ";
			}
			// default to double quotes
			else {
				ret &= '#newDoubleQuote##newDoubleQuote#  // ';
			}
		}
		else {
			//no type was defined
			ret &= '#newDoubleQuote##newDoubleQuote#  // ';
		}

	 	if (s.required) {
	 		ret &= ' REQUIRED';
	 	}

		// does the struct have a TYPE attrib?
		if (structKeyExists(s, 'TYPE')) {
		 	ret &= '  type = ' & s['TYPE'];
	 	}
		return ret;
	}










	public string function capFirstLetter(
		required string mString
		) {
		if(!len(arguments.mString)) {
			throw('zero length string cannot be argument.');
		}
		var firstChar=uCase(left(arguments.mString, 1));
		var remainingChars=removechars(arguments.mString, 1, 1);
		return firstChar & remainingChars;
	}









	public string function extractObjName(
		required string compPath	hint = "dotpath to the component"
	) {
		return listLast(arguments.compPath,'.');
	}






/* =======================================================================================================================
==  accessors and mutators ===============================================================================================
======================================================================================================================= */


	public void function setCompPath(
		required string compPath
	) {
		variables.compPath=arguments.compPath;
	}
	public string function getCompPath() {
		return variables.compPath;
	}


	public void function setCustomTestURL(
		required string customTestURL hint = "similar to /test/path/to/component or /path/test/to/component/ depending on your env."
	) {
		variables.customTestURL = arguments.customTestURL;
	}
	public string function getCustomTestURL() {
		return variables.customTestURL;
	}


	public void function setUseFactoryToCreateObject(
		required boolean useFactoryToCreateObject
	) {
		variables.useFactoryToCreateObject = arguments.useFactoryToCreateObject;
	}
	public boolean function getUseFactoryToCreateObject() {
		return variables.useFactoryToCreateObject;
	}


	public void function setCompExtends(
		required string compExtends
	) {
		variables.compExtends = arguments.compExtends;
	}
	public string function getCompExtends() {
		return variables.compExtends;
	}


}