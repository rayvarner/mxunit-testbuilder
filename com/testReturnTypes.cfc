component {

import 'mxunit-testbuilder.com.*';

	public void function test_return_void() {
	
	}
/*
	public 'mxunit-testbuilder.com.anEmpty' function test_return_component() {
		return new(mxunit-testbuilder.com.anEmpty);
	}
*/	


	public object function test_return_component() {
		var ret= new anEmpty();
		return ret;
	}
	
/*
	var typeArray=[
		'any','array','binary','boolean','component','date','guid','numeric','query','string'
		,'struct','uuid','variableName','xml','some.component.name'
	];
*/	
	public void function testArgTypes(
		required any mAny
		, required mAnyWithNoDeclaration
		, required com.anEmpty mComponentByName
		, required guid mGuid
		, required uuid mUUid
		, required variablename mVariableName
		, required numeric mSomenumber
	) {
		
	}

	public void function testArgTypesSansRequireds(
		any mAny
		, mAnyWithNoDeclaration
		, com.anEmpty mComponentByName
		, guid mGuid
		, uuid mUUid
		, variablename mVariableName
		, numeric mSomenumber
	) {
		
	}
		
	
}