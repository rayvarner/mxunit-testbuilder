component hint="I create mxunit unit tests for a struct" 
{
	
/*

SEE TESTSTRUCTTESTBUILDER.CFC FOR DETAILS!

		structTestBuilder = new "mxunit-testbuilder.com.structTestBuilder"();
		args = {		
			elems : [
				  {mName : "myArrayWithLength" , mType : "array" , mHasLengthOrVal : true}
				, {mName : "myStringWithLength" , mType : "string" , mHasLengthOrVal : true}
				, {mName : "myObj" , mType : "my.path.to.myObj" , mHasLengthOrVal : true}
				, {mName : "myNumericWithLength" , mType : "numeric" , mHasLengthOrVal : true}
				, {mName : "myUUID" , mType : "uuid" , mHasLengthOrVal : true}
				, {mName : "myGuid" , mType : "guid" , mHasLengthOrVal : true}
				, {mName : "myXml" , mType : "xml" , mHasLengthOrVal : true}
				, {mName : "myBoolean" , mType : "boolean" , mHasLengthOrVal : true}
				, {mName : "myDate" , mType : "date" , mHasLengthOrVal : true}
				, {mName : "myQuery" , mType : "query" , mHasLengthOrVal : true}
				, {mName : "myStruct" , mType : "struct" , mHasLengthOrVal : true}
				, {mName : "mybinary" , mType : "binary" , mHasLengthOrVal : true}
				, {mName : "myArraySansLength" , mType : "array" }
				, {mName : "myStringSansLength" , mType : "string" }
				, {mName : "myObjSansLength" , mType : "my.path.to.myObj" }
				, {mName : "myNumericSansLength" , mType : "numeric"}
				, {mName : "myUUIDSansLength" , mType : "uuid" }
				, {mName : "myGuidSansLength" , mType : "guid" }
				, {mName : "myXmlSansLength" , mType : "xml" }
				, {mName : "myBooleanSansLength" , mType : "boolean" }
				, {mName : "myDateSansLength" , mType : "date" }
				, {mName : "myQuerySansLength" , mType : "query" }
				, {mName : "myStructSansLength" , mType : "struct" }
				, {mName : "mybinarySansLength" , mType : "binary" }
			]
		};
		var shouldBeString = structTestBuilder.generateTests(argumentcollection = args);

RETURNS:

//debug(shouldBeStruct);
assertTrue(isValid('struct', shouldBeStruct), 'Invalid datatype. Expected struct');
	// myArrayWithLength
assertTrue(structKeyExists(shouldBeStruct, 'myArrayWithLength'), 'struct missing key: myArrayWithLength');
assertTrue(isValid('array', shouldBeStruct['myArrayWithLength']), 'Invalid datatype. Expected array');
assertFalse(arraylen(shouldBeStruct['myArrayWithLength'])==0, 'Unexpected zero length array for: shouldBeStruct[myArrayWithLength]');
	// myStringWithLength
assertTrue(structKeyExists(shouldBeStruct, 'myStringWithLength'), 'struct missing key: myStringWithLength');
assertTrue(isValid('string', shouldBeStruct['myStringWithLength']), 'Invalid datatype. Expected string');
assertFalse(len(shouldBeStruct['myStringWithLength'])==0, 'Unexpected zero length string for: shouldBeStruct[myStringWithLength]');
	// myObj
assertTrue(structKeyExists(shouldBeStruct, 'myObj'), 'struct missing key: myObj');
assertTrue(isObject(shouldBeStruct['myObj']), 'Invalid datatype. Expected object');
assertTrue(isInstanceOf(shouldBeStruct['myObj'], 'my.path.to.myObj'), 'Expected Instance of: my.path.to.myObj');
assertFalse(isNull(shouldBeStruct['myObj']), 'NULL value detected for: shouldBeStruct[myObj]');
	// myNumericWithLength
assertTrue(structKeyExists(shouldBeStruct, 'myNumericWithLength'), 'struct missing key: myNumericWithLength');
assertTrue(isValid('numeric', shouldBeStruct['myNumericWithLength']), 'Invalid datatype. Expected numeric');
assertFalse(shouldBeStruct['myNumericWithLength']==0, 'Unexpected zero value for: shouldBeStruct[myNumericWithLength]');
	// myUUID
assertTrue(structKeyExists(shouldBeStruct, 'myUUID'), 'struct missing key: myUUID');
assertTrue(isValid('uuid', shouldBeStruct['myUUID']), 'Invalid datatype. Expected uuid');
assertFalse(len(shouldBeStruct['myUUID'])==0, 'Unexpected zero length string for: shouldBeStruct[myUUID]');
	// myGuid
assertTrue(structKeyExists(shouldBeStruct, 'myGuid'), 'struct missing key: myGuid');
assertTrue(isValid('guid', shouldBeStruct['myGuid']), 'Invalid datatype. Expected guid');
assertFalse(len(shouldBeStruct['myGuid'])==0, 'Unexpected zero length string for: shouldBeStruct[myGuid]');
	// myXml
assertTrue(structKeyExists(shouldBeStruct, 'myXml'), 'struct missing key: myXml');
assertTrue(isValid('xml', shouldBeStruct['myXml']), 'Invalid datatype. Expected xml');
assertFalse(len(shouldBeStruct['myXml'])==0, 'Unexpected zero length string for: shouldBeStruct[myXml]');
	// myBoolean
assertTrue(structKeyExists(shouldBeStruct, 'myBoolean'), 'struct missing key: myBoolean');
assertTrue(isValid('boolean', shouldBeStruct['myBoolean']), 'Invalid datatype. Expected boolean');
assertFalse(isNull(shouldBeStruct['myBoolean']), 'NULL value detected for: shouldBeStruct[myBoolean]');
	// myDate
assertTrue(structKeyExists(shouldBeStruct, 'myDate'), 'struct missing key: myDate');
assertTrue(isValid('date', shouldBeStruct['myDate']), 'Invalid datatype. Expected date');
assertFalse(isNull(shouldBeStruct['myDate']), 'NULL value detected for: shouldBeStruct[myDate]');
	// myQuery
assertTrue(structKeyExists(shouldBeStruct, 'myQuery'), 'struct missing key: myQuery');
assertTrue(isValid('query', shouldBeStruct['myQuery']), 'Invalid datatype. Expected query');
assertTrue(shouldBeStruct['myQuery'].recordCount > 0, 'Unexpected zero length query for: shouldBeStruct[myQuery]');
	// myStruct
assertTrue(structKeyExists(shouldBeStruct, 'myStruct'), 'struct missing key: myStruct');
assertTrue(isValid('struct', shouldBeStruct['myStruct']), 'Invalid datatype. Expected struct');
assertFalse(structIsEmpty(shouldBeStruct['myStruct']), 'Unexpected empty struct for: shouldBeStruct[myStruct]');
	// mybinary
assertTrue(structKeyExists(shouldBeStruct, 'mybinary'), 'struct missing key: mybinary');
assertTrue(isValid('binary', shouldBeStruct['mybinary']), 'Invalid datatype. Expected binary');
assertFalse(isNull(shouldBeStruct['mybinary']), 'NULL value detected for: shouldBeStruct[mybinary]');
	// myArraySansLength
assertTrue(structKeyExists(shouldBeStruct, 'myArraySansLength'), 'struct missing key: myArraySansLength');
assertTrue(isValid('array', shouldBeStruct['myArraySansLength']), 'Invalid datatype. Expected array');
	// myStringSansLength
assertTrue(structKeyExists(shouldBeStruct, 'myStringSansLength'), 'struct missing key: myStringSansLength');
assertTrue(isValid('string', shouldBeStruct['myStringSansLength']), 'Invalid datatype. Expected string');
	// myObjSansLength
assertTrue(structKeyExists(shouldBeStruct, 'myObjSansLength'), 'struct missing key: myObjSansLength');
assertTrue(isObject(shouldBeStruct['myObjSansLength']), 'Invalid datatype. Expected object');
assertTrue(isInstanceOf(shouldBeStruct['myObjSansLength'], 'my.path.to.myObj'), 'Expected Instance of: my.path.to.myObj');
	// myNumericSansLength
assertTrue(structKeyExists(shouldBeStruct, 'myNumericSansLength'), 'struct missing key: myNumericSansLength');
assertTrue(isValid('numeric', shouldBeStruct['myNumericSansLength']), 'Invalid datatype. Expected numeric');
	// myUUIDSansLength
assertTrue(structKeyExists(shouldBeStruct, 'myUUIDSansLength'), 'struct missing key: myUUIDSansLength');
assertTrue(isValid('uuid', shouldBeStruct['myUUIDSansLength']), 'Invalid datatype. Expected uuid');
	// myGuidSansLength
assertTrue(structKeyExists(shouldBeStruct, 'myGuidSansLength'), 'struct missing key: myGuidSansLength');
assertTrue(isValid('guid', shouldBeStruct['myGuidSansLength']), 'Invalid datatype. Expected guid');
	// myXmlSansLength
assertTrue(structKeyExists(shouldBeStruct, 'myXmlSansLength'), 'struct missing key: myXmlSansLength');
assertTrue(isValid('xml', shouldBeStruct['myXmlSansLength']), 'Invalid datatype. Expected xml');
	// myBooleanSansLength
assertTrue(structKeyExists(shouldBeStruct, 'myBooleanSansLength'), 'struct missing key: myBooleanSansLength');
assertTrue(isValid('boolean', shouldBeStruct['myBooleanSansLength']), 'Invalid datatype. Expected boolean');
	// myDateSansLength
assertTrue(structKeyExists(shouldBeStruct, 'myDateSansLength'), 'struct missing key: myDateSansLength');
assertTrue(isValid('date', shouldBeStruct['myDateSansLength']), 'Invalid datatype. Expected date');
	// myQuerySansLength
assertTrue(structKeyExists(shouldBeStruct, 'myQuerySansLength'), 'struct missing key: myQuerySansLength');
assertTrue(isValid('query', shouldBeStruct['myQuerySansLength']), 'Invalid datatype. Expected query');
	// myStructSansLength
assertTrue(structKeyExists(shouldBeStruct, 'myStructSansLength'), 'struct missing key: myStructSansLength');
assertTrue(isValid('struct', shouldBeStruct['myStructSansLength']), 'Invalid datatype. Expected struct');
	// mybinarySansLength
assertTrue(structKeyExists(shouldBeStruct, 'mybinarySansLength'), 'struct missing key: mybinarySansLength');
assertTrue(isValid('binary', shouldBeStruct['mybinarySansLength']), 'Invalid datatype. Expected binary');
*/
variables.newLine = 			chr(10);// & chr(13);
variables.newTab = 				chr(9);
variables.newDoubleQuote = 		chr(34);
variables.newSingleQuote = 		chr(39);
variables.enforceType = 		true;
	
	public any function init(
		string defaultVariableName = 'shouldBeStruct'	// default variable name for the parent struct we're creating tests for.
		, boolean enforceType = true					// if true, will add tests for datatype and/or values
	) {
		structAppend(variables, arguments, true);
		return this;
	}

	/**
	* I take an array of structs that will be interpreted into tests
	*/
	public string function generateTests(
		required array elems
	) {
	var ret="//debug(#variables.defaultVariableName#);" & newLine;
		ret &= "assertTrue(isValid('struct', #variables.defaultVariableName#), 'Invalid datatype. Expected struct');" & newLine;
		for(var e in arguments.elems) {
			ret &= variables.newTab & "// " & e.mName & newLine;
			ret &= _makeTestForStruct(argumentCollection = e);
		}
		return ret;
	}

	/**
	* I take a struct and create a unique set of UT based on included keys
	*/
	private string function _makeTestForStruct(
		  required string mName
		, string mType='any'
		, boolean mHasLengthOrVal=false
		, string shouldEq = '' 
		) {
		var ret='';
		// key Exists
		ret &= "assertTrue(structKeyExists(#variables.defaultVariableName#, '#mName#'), 'struct missing key: #mName#');" & newLine;
		if(variables.enforceType) {
			// the datatype comes before length
			if(arrayFindNoCase(['guid','string','uuid','xml','array','struct','query','numeric'],arguments.mType)) {
				// key datatype
				ret &= _getTypeForIsValid(mName="#variables.defaultVariableName#['#arguments.mName#']", mType=trim(arguments.mType)) & newLine;
				// key value/length
				if(mHasLengthOrVal) {
					ret &= _getLengthOrValue(mName="#variables.defaultVariableName#['#arguments.mName#']", mType=trim(arguments.mType)) & newLine;
				}
			}
			// otherwise length before datatype
			else {
				// key value/length
				if(mHasLengthOrVal) {
					ret &= _getLengthOrValue(mName="#variables.defaultVariableName#['#arguments.mName#']", mType=trim(arguments.mType)) & newLine;
				}
				// key datatype
				ret &= _getTypeForIsValid(mName="#variables.defaultVariableName#['#arguments.mName#']", mType=trim(arguments.mType)) & newLine;
			}
			// key value to evaluate
			if(len(shouldEq)) {
				ret &= _getShouldEq(mName="#variables.defaultVariableName#['#arguments.mName#']", shouldEq=shouldEq)& newLine;
			}
		}
		return ret;	
	}

	/**
	* I create a test for an exact value
	*/
	private string function _getShouldEq(
		required string mName
		, required string shouldEq
	) {
		var ret='';
		var nameForOutput = reReplace(arguments.mName, '#newDoubleQuote#|#newSingleQuote#', '','all');
		ret &= "assertTrue(#arguments.mName# == '#arguments.shouldEq#' , 'Expected: #nameForOutput# == " & arguments.shouldEq & " ');";
		return ret;
	}
	
	/**
	* I create a basic test based on the datatype. If string, must pass len. If array, must pass arrayLen.
	*/
	private string function _getLengthOrValue(
		required string mName
		, required string mType
	) {
		var ret='';
		var nameForOutput = reReplace(arguments.mName, '#newDoubleQuote#|#newSingleQuote#', '','all');
		// strings
		if(arrayFindNoCase(['guid','string','uuid','xml'],arguments.mType)) {
			ret &= "assertFalse(len(#arguments.mName#)==0, 'Unexpected zero length string for: #nameForOutput#');";
		}
		else if(arrayFindNoCase(['numeric'],arguments.mType)) {
			ret &= "assertFalse(#arguments.mName#==0, 'Unexpected zero value for: #nameForOutput#');";
		}
		else if(arguments.mType=='array') {
			ret &= "assertFalse(arraylen(#arguments.mName#)==0, 'Unexpected zero length array for: #nameForOutput#');";
		}
		else if(arguments.mType=='struct') {
			ret &= "assertFalse(structIsEmpty(#arguments.mName#), 'Unexpected empty struct for: #nameForOutput#');";
		}
		else if(arguments.mType=='query') {
			ret &= "assertTrue(#arguments.mName#.recordCount > 0, 'Unexpected zero length query for: #nameForOutput#');";
		}
		else {
			ret &= "assertFalse(isNull(#arguments.mName#), 'NULL value detected for: #nameForOutput#');";
		}
		return ret;
	}
	
	/**
	* I create an ISVALID test based on the datatype passed in. 
	*/
	private string function _getTypeForIsValid(
		required string mName
		, required string mType
	) {
		var ret='';
		if(arrayFindNoCase(['array','binary','boolean','date','guid','numeric','query','string','struct','uuid','xml'], arguments.mType)) {
			ret = "assertTrue(isValid('#arguments.mType#', #arguments.mName#), 'Invalid datatype. Expected #arguments.mType#');";
		}
		// assume dotpath component definition. ex: path.to.component
		else {
			ret = "assertTrue(isObject(#arguments.mName#), 'Invalid datatype. Expected object');" & newLine;
			ret &= "assertTrue(isInstanceOf(#arguments.mName#, '#arguments.mType#'), 'Expected Instance of: #arguments.mType#');";
		}
		return ret;
	}
	
	
}