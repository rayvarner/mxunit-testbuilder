<cfsilent><cfprocessingdirective pageEncoding= "UTF-8" />
<cfsavecontent variable="myArrayOfStructsString" >[
  {mName : "success" , mType : "boolean", shouldEq: 'true'}
, {mName : "errors" , mType : "array"}
, {mName : "employeeId" , mType : "string" , mHasLengthOrVal : true}
, {mName : "user" , mType : "wc.beans.user", mHasLengthOrVal : true}
, {mName : "bdate" , mType : "date", mHasLengthOrVal : true}
]</cfsavecontent>
<cfscript>
if(structKeyExists(form,'arrayOfStructsAsString') && len(form.arrayOfStructsAsString)) {
	myArrayOfStructsString = form.arrayOfStructsAsString;
}


param name="form.arrayOfStructsAsString" default="#myArrayOfStructsString#";



structTestBuilder = new "mxunit-testbuilder.com.structTestBuilder"();
		args = {
			elems : evaluate(form.arrayOfStructsAsString)
		};
		rslt = structTestBuilder.generateTests(argumentcollection = args);
</cfscript>

</cfsilent><!DOCTYPE html> 
<html lang=en>
<head>
	<title>MXUnit - Make for Struct</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
<style type="text/css" media="screen">
    #myStuff {width: 800px;}
    #myResult {height: 300px;width: 800px;}
    #arrayOfStructsAsString {width:700px;height:150px;}
    #bigcontainer {width:850px;margin:50px;}
</style>
</head>

<body>
<div id="bigcontainer">

<nav class="navbar navbar-inverse">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="##">MXUnit - Test Builder</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="index.cfm">Main</a></li>
              <li class="active"><a href="##" >Make for Struct</a></li>
              <li><a href="unittest/runner.cfm" target="_blank">Test Runner</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>
	


<cfoutput>
	

	<div class="panel panel-info">
	    <div class="panel-heading">
	      <div class="panel-title">Example struct w/in the array:</div>
	    </div>
	    <div class="panel-body">
			<div class="row">
				<div class="col-md-1"><code>{</code></div>
			</div>
			<div class="row">
				<div class="col-md-11 col-md-offset-1"><code>mName : "structKeyName"</code> <small>// Name of the key within the struct to test</small></div>
			</div>
			<div class="row">
				<div class="col-md-11 col-md-offset-1"><code>, mType : "type"</code> <small>//array,binary,boolean,date,guid,numeric,query,string,struct,uuid,xml, or dot-path-to-component</small></div>
			</div>			
			<div class="row">
				<div class="col-md-11 col-md-offset-1"><code>, mHasLengthOrVal : true</code> <small>// If true: numeric can't be zero; struct & array can't be empty; string can't be empty</small></div>
			</div>		
			<div class="row">
				<div class="col-md-11 col-md-offset-1"><code>, shouldEq: 'true'</code> <small>// If provided, will populate (as a string) where mName == <var>'##shouldEq##'</var></small></div>
			</div>		
 			<div class="row">
				<div class="col-md-1"><code>}</code></div>
			</div>
	    </div>
	  </div>
	  


	<div class="panel panel-primary">
		<div class="panel-heading">
		  <div class="panel-title">Input your test creation data.</div>
		</div>
		<div class="panel-body">
			<form action="#cgi.script_name#" method="post">
				<div class="row">
					<div class="col-md-12">
						<textarea class="form-field" id="arrayOfStructsAsString" name="arrayOfStructsAsString">#(myArrayOfStructsString)#</textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">&nbsp;</div>
					<div class="col-md-4">
						<input class="btn btn-success" type="submit" value="Generate" name="generate" />
					</div>
				</div>
			</form>
		</div>
	</div>
          
          


<hr />


	<div class="panel panel-success">
		<div class="panel-heading">
		  <h3 class="panel-title">Server Generated Code</h3>
		</div>
		<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div id="myResult">&lt;cfscript&gt;
#(rslt)#
&lt;/cfscript&gt;</div>
					</div>
				</div>
		</div>
	</div>

<!---<textarea style="width:800px;height:400px;">#(rslt)#</textarea>--->

</cfoutput>

<!---<cfdump var="#cBuilder.getTheMetaData()#" />--->


	<div class="panel panel-default">
		<div class="panel-heading">
		  <h3 class="panel-title">Data-Type Reference</h3>
		</div>
		<div class="panel-body">
			<div id="myStuff" class="pre-scrollable"><pre><code>[
  {mName : "myArrayWithLength" , mType : "array" , mHasLengthOrVal : true}
, {mName : "myStringWithLength" , mType : "string" , mHasLengthOrVal : true}
, {mName : "myObj" , mType : "my.path.to.myObj" , mHasLengthOrVal : true}
, {mName : "myNumericWithLength" , mType : "numeric" , mHasLengthOrVal : true}
, {mName : "myUUID" , mType : "uuid" , mHasLengthOrVal : true}
, {mName : "myGuid" , mType : "guid" , mHasLengthOrVal : true}
, {mName : "myXml" , mType : "xml" , mHasLengthOrVal : true}
, {mName : "myBoolean" , mType : "boolean" , mHasLengthOrVal : true}
, {mName : "myDate" , mType : "date" , mHasLengthOrVal : true}
, {mName : "myQuery" , mType : "query" , mHasLengthOrVal : true}
, {mName : "myStruct" , mType : "struct" , mHasLengthOrVal : true}
, {mName : "mybinary" , mType : "binary" , mHasLengthOrVal : true}
, {mName : "myArraySansLength" , mType : "array" }
, {mName : "myStringSansLength" , mType : "string" }
, {mName : "myObjSansLength" , mType : "my.path.to.myObj" }
, {mName : "myNumericSansLength" , mType : "numeric"}
, {mName : "myUUIDSansLength" , mType : "uuid" }
, {mName : "myGuidSansLength" , mType : "guid" }
, {mName : "myXmlSansLength" , mType : "xml" }
, {mName : "myBooleanSansLength" , mType : "boolean" }
, {mName : "myDateSansLength" , mType : "date" }
, {mName : "myQuerySansLength" , mType : "query" }
, {mName : "myStructSansLength" , mType : "struct" }
, {mName : "mybinarySansLength" , mType : "binary" }
]</code></pre></div>
		</div>
	</div>


</div><!-- /bigcontainer -->
			
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.5/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
    var myResultEditor = ace.edit("myResult");
    myResultEditor.setTheme("ace/theme/monokai");
    myResultEditor.getSession().setMode("ace/mode/coldfusion");
</script>
</body>
</html>