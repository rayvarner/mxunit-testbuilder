<cfsilent>
<cfscript>
	/*
	http://getbootstrap.com/getting-started/
	http://getbootstrap.com/css/#type
	*/
	args = {};
	structAppend(args, form, true );
	
	param name="args.compPath" default="mxunit-testbuilder.com.ReturnTypesTester";
	param name="args.compExtends" default="mxunit.framework.TestCase"; 				// 'test.wc.base'	// default: "mxunit.framework.TestCase"
	param name="args.customTestURL" default="/mxunit-testbuilder/unittest/com/"; 	// this will determine where the file is written & the url to the generated file
	param name="args.useFactoryToCreateObject" default="false";						// default: false
	param name="args.skipInit" default="true";										// if the function is named INIT, we do not want a test case for it
	param name="args.writeToFileSystem" default="false";
	param name="args.forceOverwrite" default="false";
	param name="args.failAllTests" default="true";
	
	cBuilder = new "mxunit-testbuilder.com.cBuilder"();
	try {
		rslt = cBuilder.process(argumentCollection = args);		
	}
	catch('custom' e) {
		writeDump(e.message);
		abort;
	}
	//writeDump(getMetaData(rslt).getModifiers());
</cfscript>

</cfsilent><!DOCTYPE html> 
<html lang=en>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<title>MXUnit - Test Builder</title>
	<style>
		.ti {width:400px;}
		#args {width:650px;}
		#go {width:100px;}
		#fileContent {width:800px;height:400px;}
		#bigcontainer {width:850px;margin:50px;}
	    #myResult {
	    	 height: 800px;
	    	 width: 800px;
	    }
		.pull-left {
		  float: left !important;
		}
		.pull-right {
		  float: right !important;
		}
	</style>
</head>
<body>
<!---
<cfdump var="#args#" expand="false" label="Arguments to cBuilder.process()" />
<cfdump var="#rslt#" expand="false" label="Results from cBuilder.process()" />
--->
<cfoutput>
	
<form  method="post" name="frm" action="#cgi.SCRIPT_NAME#">

	
<div id="bigcontainer">
	

	
	
	
	
	
	
<nav class="navbar navbar-inverse">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="##">MXUnit - Test Builder</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="##">Main</a></li>
              <li><a href="MakeTestsForStruct.cfm">Make for Struct</a></li>
              <li><a href="unittest/runner.cfm" target="_blank">Test Runner</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>
	
	
	
	
	
	
	<div class="panel panel-primary">
	    <div class="panel-heading">
	      <div class="panel-title">Input your test creation data.</div>
	    </div>
	    <div class="panel-body">
	
			<!-- start TR -->
			<div class="row">
				<div class="col-md-4"><label for="compPath">Component Path</label></div>
				<div class="col-md-8"><input type="text" class="form-control ti" name="compPath" value="#args.compPath#"  placeholder="path.to.component" /></div>
			</div>
			<div class="row">
				<div class="col-md-4"><label for="customTestURL">Custom Test URL</label></div>
				<div class="col-md-8"><input type="text" class="form-control ti" name="customTestURL" value="#args.customTestURL#" placeholder="/mxunittest/path/to/" /></div>
			</div>
			<div class="row">
				<div class="col-md-4"><label for="compExtends">Component Extends</label></div>
				<div class="col-md-8"><input type="text" class="form-control ti" name="compExtends" value="#args.compExtends#" placeholder="mxunit.framework.TestCase" /></div>
			</div>
			<div class="row">
				<div class="col-md-4"><label for="useFactoryToCreateObject">Use Factory to Create Object</label></div>
				<div class="col-md-8">
					<input type="radio" name="useFactoryToCreateObject" value="true" #((args.useFactoryToCreateObject) ? "checked='checked'" : "")# /> true
					<br />
					<input type="radio" name="useFactoryToCreateObject" value="false" #((!args.useFactoryToCreateObject) ? "checked='checked'" : "")# /> false
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"><label for="skipInit">Skip Init funct</label></div>
				<div class="col-md-8">
					<input type="radio" name="skipInit" value="true" #((args.skipInit) ? "checked='checked'" : "")# /> true
					<br />
					<input type="radio" name="skipInit" value="false" #((!args.skipInit) ? "checked='checked'" : "")# /> false
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"><label for="writeToFileSystem">Write to File System</label></div>
				<div class="col-md-8">
					<input type="radio" name="writeToFileSystem" value="true" #((args.writeToFileSystem) ? "checked='checked'" : "")# /> true
					<br />
					<input type="radio" name="writeToFileSystem" value="false" #((!args.writeToFileSystem) ? "checked='checked'" : "")# /> false
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"><label for="forceOverwrite">Force Overwrite</label></div>
				<div class="col-md-8">
					<input type="radio" name="forceOverwrite" value="true" #((args.forceOverwrite) ? "checked='checked'" : "")# /> true
					<br />
					<input type="radio" name="forceOverwrite" value="false" #((!args.forceOverwrite) ? "checked='checked'" : "")# /> false
				</div>
			</div>
			<div class="row">
				<div class="col-md-4"><label for="failAllTests" >Fail All Tests</label></div>
				<div class="col-md-8">
					<input type="radio" name="failAllTests" value="true" #((args.failAllTests) ? "checked='checked'" : "")# /> true
					<br />
					<input type="radio" name="failAllTests" value="false" #((!args.failAllTests) ? "checked='checked'" : "")# /> false
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">&nbsp;</div>
				<div class="col-md-4">
					<input class="btn btn-success" type="submit" id="go" name="go" value="Generate" />
				</div>
			</div>																		
			<!-- end TR -->
	
	    </div>
	  </div>
  
  
	<cfif structKeyexists(rslt, 'WriteFile') AND isStruct(rslt['WriteFile']) AND NOT structIsEmpty(rslt['WriteFile'])>
		<br />
		<div class="row">
			#rslt['WriteFile'].file_write_result#
		</div>
		<div class="row">
			#rslt['WriteFile'].canonical_path_and_file#
		</div>
		<div class="row">
			<a class="btn btn-danger" href = "#rslt['WriteFile'].http_url#" target="_blank">#rslt['WriteFile'].http_url#</a>
		</div>
	</cfif>
  


<div class="panel panel-success">
	<div class="panel-heading">
		<h2 class="panel-title"><label for="fileContent">Server Generated Code</label></h2>
	</div>
	<div class="panel-body">
		<!---<textarea class="form-control" id="fileContent" name="fileContent">#(rslt.fileContent)#</textarea>--->
		<div id="myResult">&lt;cfscript&gt;
#(rslt.fileContent)#
&lt;/cfscript&gt;</div>
	</div>
</div>
	
</div><!-- /bigcontainer -->
	
</form>
</cfoutput>

				
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.5/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
    var editor = ace.edit("myResult");
    //editor.setTheme("ace/theme/monokai");
    editor.setTheme("ace/theme/solarized_light");
    editor.getSession().setMode("ace/mode/coldfusion");
</script>
</body>
</html>