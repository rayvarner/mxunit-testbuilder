component {
    this.name='mxunit-testbuilder-tests_' & hash(getCurrentTemplatePath());
    this.applicationtimeout = createTimeSpan(0,0,10,0);
    this.sessionmanagement = false;
    this.mappings['/mxunit-testbuilder'] = expandPath('/mxunit-testbuilder');

	public void function onError(
		struct exception
		,string eventname
	) {
		writeDump(arguments);
        writeLog(serializeJSON(arguments));
		abort;
	}

}