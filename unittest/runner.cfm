<cfsetting requesttimeout="5000" />
<cfscript>
args={
	component='mxunit.runner.DirectoryTestSuite'
	, method='run'
	, componentPath='mxunit-testbuilder.unittest'
	, directory = expandPath("/mxunit-testbuilder/unittest/")
	, recurse = true
	, returnVariable = "results"
};
</cfscript>



<cfinvoke attributeCollection="#args#" />

<!---
<cfset output = results.getResultsOutput( ['html', 'extjs', 'xml', 'junitxml', 'query', 'array'] ) />
 --->
<cfoutput>#results.getResultsOutput('html')#</cfoutput>